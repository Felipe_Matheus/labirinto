const container = document.getElementById("labirinto_container");
const map = [
  "WWWWWWWWWWWWWWWWWWWWW",
  "W   W     W     W W W",
  "W W W WWW WWWWW W W W",
  "W W W   W     W W   W",
  "W WWWWWWW W WWW W W W",
  "W         W     W W W",
  "W WWW WWWWW WWWWW W W",
  "W W   W   W W     W W",
  "W WWWWW W W W WWW W F",
  "S     W W W W W W WWW",
  "WWWWW W W W W W W W W",
  "W     W W W   W W W W",
  "W WWWWWWW WWWWW W W W",
  "W       W       W   W",
  "WWWWWWWWWWWWWWWWWWWWW",
];
let vertical = 9;
let horizontal = 0;
const createMap = () => {
  for (let i = 0; i < map.length; i++) {
    let linha = document.createElement("div");
    linha.className = "linhas";
    for (let j = 0; j < map[i].length; j++) {
      let celulaId = i + "." + j;
      if (map[i][j] === "W") {
        let parede = document.createElement("div");
        parede.className = "parede";
        parede.id = celulaId;
        linha.appendChild(parede);
      } else if (map[i][j] === " ") {
        let espaco = document.createElement("div");
        espaco.className = "espaco";
        espaco.id = celulaId;
        linha.appendChild(espaco);
      } else if (map[i][j] === "S") {
        let start = document.createElement("div");
        start.className = "start";
        start.id = celulaId;
        linha.appendChild(start);
      } else if (map[i][j] === "F") {
        let final = document.createElement("div");
        final.className = "final";
        final.id = celulaId;
        linha.appendChild(final);
      }
    }
    container.appendChild(linha);
  }
};
createMap();

const posicaoPlayer = () => {
  let player = document.querySelector(".player");
  let celula = vertical + "." + horizontal;
  let espacos = document.getElementById(celula);
  player.parentNode.removeChild(player);
  espacos.appendChild(player);
};
const andar = document.addEventListener("keydown", (event) => {
  let keyName = event.key;
  if (!vitoria()) {
    if (keyName === "ArrowRight" && checarMovimento(horizontal + 1, vertical)) {
      horizontal += 1;
    } else if (
      keyName === "ArrowLeft" &&
      checarMovimento(horizontal - 1, vertical)
    ) {
      horizontal -= 1;
    } else if (
      keyName === "ArrowUp" &&
      checarMovimento(horizontal, vertical - 1)
    ) {
      vertical -= 1;
    } else if (
      keyName === "ArrowDown" &&
      checarMovimento(horizontal, vertical + 1)
    ) {
      vertical += 1;
    }
  } else {
    return false;
  }
  posicaoPlayer();
  vitoria();
}); //tenho que fazer uma condição que pegue true ou false da funcao posicaPlayer
//usar isso para ver quando tiver paredes para ele não somar mais
//Ver quando ele está somando no console.log e verificar se a condicao esta batendo
//Arrumar a condicao de vitoria

posicaoPlayer();

const vitoria = () => {
  let player = document.querySelector(".player");
  if (map[vertical][horizontal] == "F") {
    let vitoria = document.getElementById("image");
    vitoria.className = "vitoria";
    player.parentNode.removeChild(player);
    let palco = document.getElementById("medal");
    palco.className = "medalha";
    let som = new Audio("vinheta-galvao-do-tetra.mp3");
    som.play();
    return true;
  }
  return false;
};
const checarMovimento = (x, y) => {
  if (
    y >= 0 &&
    y < map.length &&
    x >= 0 &&
    x < map[0].length &&
    map[y][x] !== "W"
  ) {
    return true;
  }
  return false;
};
